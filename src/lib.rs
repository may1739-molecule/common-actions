extern crate serde;
#[macro_use]
extern crate serde_derive;

pub const FILE_ACTION: &'static str = "FILE";

#[derive(Serialize, Deserialize)]
pub enum FileActionData {
    List,
    Get(String),
}